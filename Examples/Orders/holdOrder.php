<?php
/**
 * holdOrder.php
 *
 * @author      Back In Black Inc.
 * @package     bibi_api_sdk
 * @copyright   2005-2020 Back In Back Solutions
 * @update      Last Updated by Larry 4/14/2020 1:22 PM
 * @link        http://bibisolutions.net
 * @version     ${VERSION}
 *
 */



// Display errors for testing purposes
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

define('PUBLIC_KEY', '');
define('SECRET_KEY', '');


require_once '../../bibi_php_sdk/bibi_sdk.php';


// Initialize the API with public and secret keys
$api = new Bibi_SDK(PUBLIC_KEY, SECRET_KEY);

// Authenticate and grab token
$tokenResponse = $api->Authenticate();

// Handle token response automatically
$api->HandleTokenResponse($tokenResponse);
if(isset($_POST['hold_order']) && $_POST['hold_order'] !== ''){
    $holdOrderResult = $api->holdOrder($_POST['hold_order']);
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet">
    <title>Hold Order</title>
</head>

<body>

<div class="container">
    <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <h2>Hold Order</h2>
        </div>
    </div>
    <form method="post">
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                <div class="form-group">
                    <label for="hold_order" class="col-sm-2 control-label">Order Number</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" id="hold_order" placeholder="Order #"
                               value="<?php echo (isset($_POST['hold_order'])) ? $_POST['hold_order'] : ""; ?>">
                    </div>
                </div>

            </div>

        </div>
    </form>

    <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">

                <?php
                if (isset($holdOrderResult)) {
                    ?>
                        <label for="result">Result</label>
                    <textarea style="width=90%;height:300px;" id="result">
                        <?php echo json_encode($holdOrderResult, JSON_PRETTY_PRINT); ?></textarea>
                    <?php
                }
            ?>
        </div>
    </div>
</div>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
</body>
</html>