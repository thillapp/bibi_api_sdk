<?php
/**
 * listProducts.php
 *
 * @author      Back In Black Inc.
 * @package     bibisolutions_api_php
 * @copyright   2005-2018 Back In Back Solutions
 * @update      Last Updated by Larry Bislew 11/21/2018 11:02 AM
 * @link        http://bibisolutions.net
 * @version     2.04.1
 *
 */


// Display errors for testing purposes
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

define('PUBLIC_KEY','');
define('SECRET_KEY','');

require_once '../../bibi_php_sdk/bibi_sdk.php';

// Initialize the API with public and secret keys
$api = new Bibi_SDK(PUBLIC_KEY, SECRET_KEY);

// Authenticate and grab token
$tokenResponse = $api->Authenticate();

// Handle token response automatically
$api->HandleTokenResponse($tokenResponse);

$parameters = array();

// Optional Parameters - Omit if not needed or comment out.
$parameters['offset'] = 1;
$parameters['perPage'] = 100; // Limited to 100
$parameters['min_qty'] = 1; // Must have at least the many in stock
$parameters['max_qty'] = 10; // Must have at most his number in stock


$response = $api->listProducts($parameters);

echo json_encode($response, JSON_PRETTY_PRINT);