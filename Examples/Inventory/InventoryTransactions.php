<?php
/**
 * InventoryTransactions.php
 *
 * @author      Back In Black Inc.
 * @package     bibi_api_sdk
 * @copyright   2005-2021 Back In Back Solutions
 * @update      Last Updated by Larry 7/20/2021 2:14 PM
 * @link        http://bibisolutions.net
 * @version     2.07.0
 *
 */


// Display errors for testing purposes
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

define('PUBLIC_KEY', '');
define('SECRET_KEY', '');
require_once '../../bibi_php_sdk/bibi_sdk.php';

// Initialize the API with public and secret keys
$api = new Bibi_SDK(PUBLIC_KEY, SECRET_KEY);

// Authenticate and grab token
$tokenResponse = $api->Authenticate();
// Handle token response automatically
$api->HandleTokenResponse($tokenResponse);

$dateStart = '';
$dateEnd = '';
$productNumber = '';
$transactionType = [];
if (isset($_POST) && is_array($_POST)) {
    if (isset($_POST['date_start'])) $dateStart = $_POST['date_start'];
    if (isset($_POST['date_end'])) $dateEnd = $_POST['date_end'];
    if (isset($_POST['product_number'])) $productNumber = $_POST['product_number'];
    if (isset($_POST['transaction_type']) && is_array($_POST['transaction_type'])) {
        $transactionType = $_POST['transaction_type'];
    }
}
$transactions = $api->inventoryTransactions($dateStart, $dateEnd, $productNumber, $transactionType);
$types = $api->inventoryTransactionTypes();
$reasons = $api->inventoryResons();

?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet">
    <title>Inventory Transactions</title>
</head>

<body>

<form action="" method="post" class="form-horizontal" role="form">
    <div class="container">
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <h2>InventoryTransactions</h2>
            </div>
        </div>
        <?php
        if (isset($api)) {
            ?>
            <div class="col-xs-7 col-sm-7 col-md-7 col-lg-7">

                <div class="form-group">
                    <label for="date_start" class="col-sm-4 control-label">Date Start</label>
                    <div class="col-sm-8">
                        <input type="text" class="form-control" id="date_start" name="date_start" placeholder="" value="<?php echo $dateStart; ?>">
                    </div>
                </div>
                <div class="form-group">
                    <label for="date_end" class="col-sm-4 control-label">Date End</label>
                    <div class="col-sm-8">
                        <input type="text" class="form-control" id="date_end" name="date_end" placeholder="" value="<?php echo $dateEnd; ?>">
                    </div>
                </div>
                <div class="form-group">
                    <label for="product_number" class="col-sm-4 control-label">Product_number</label>
                    <div class="col-sm-8">
                        <input type="text" class="form-control" id="product_number" name="product_number" placeholder="" value="<?php echo $productNumber; ?>">
                    </div>
                </div>

            </div>
            <div class="col-xs-5 col-sm-5 col-md-5 col-lg-5">
                <div class="form-group">
                    <?php
                    foreach ($types['result'] as $type) {

                        ?>
                        <div class="col-sm-offset-2 col-sm-10">
                            <div class="checkbox">
                                <label>
                                    <input type="checkbox" value="<?php echo $type['transaction_type']; ?>"
                                           name="transaction_type[]"
                                        <?php echo (in_array($type['transaction_type'], $transactionType)) ? 'CHECKED' : ''; ?>>
                                </label> <?php echo $type['transaction_type'] . ' - ' . $type['description']; ?>
                            </div>
                        </div>
                        <?php

                    }
                    ?>
                </div>

            </div>

            <div class="col-lg-4 pull-right">
                <button type="submit" class="btn btn-primary">Process</button>
            </div>
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <div class="table-responsive">
                    <?php
                    if (isset($transactions) && is_array($transactions['result']) && count($transactions['result']) > 0) {
                        $columns = array_keys($transactions['result'][0]);
                        ?>
                        <table class="table table-hover">
                            <thead>
                            <tr>
                                <?php
                                foreach ($columns as $column) {
                                    ?>
                                    <th>
                                        <?php
                                        echo $column;
                                        ?>
                                    </th>
                                    <?php
                                }
                                ?>
                            </tr>
                            </thead>
                            <tbody>
                            <?php
                            foreach ($transactions['result'] as $transaction) {
                                ?>
                                <tr>
                                    <?php
                                    foreach ($columns as $column) {
                                        ?>
                                        <td>
                                            <?php
                                            echo (isset($transaction[$column])) ? ucwords(str_replace("_"," ",$transaction[$column])) : '';
                                            ?>
                                        </td>
                                        <?php
                                    }
                                    ?>
                                </tr>
                                <?php
                            }
                            ?>
                            </tbody>
                        </table>
                        <?php
                    }
                    ?>
                    </div>
                </div>
            </div>
            <h2>Reason Codes</h2>
            <pre>
            <?php
            echo json_encode($reasons['result'], JSON_PRETTY_PRINT);
            ?>
        </pre>
            <br/>
            <br/>
            <h2>Transaction Types</h2>
            <pre>
            <?php
            echo json_encode($types['result'], JSON_PRETTY_PRINT);
            ?>
        </pre>
            <?php
        }
        ?>
    </div>

</form>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
</body>
</html>