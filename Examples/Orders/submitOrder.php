<?php
/**
 * submitOrder.php
 *
 * @author      Back In Black Inc.
 * @package     bibisolutions_api_php
 * @copyright   2005-2018 Back In Back Solutions
 * @update      Last Updated by Larry Bislew 8/20/2018 12:28 PM
 * @link        http://bibisolutions.net
 * @version     2.04.1
 *
 */
// Display errors for testing purposes
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

define('PUBLIC_KEY','');
define('SECRET_KEY','');

require_once '../../bibi_php_sdk/bibi_sdk.php';

// Initialize the API with public and secret keys
$api = new Bibi_SDK(PUBLIC_KEY, SECRET_KEY);

// Authenticate and grab token
$tokenResponse = $api->Authenticate();

// Handle token response automatically
$api->HandleTokenResponse($tokenResponse);


/* Sample order data */
$order_data = '{
    "Test": "T",
    "Orders": [
        {
            "Order": {
                "Header": {
                    "HeaderGroup": {
                        "OrderNumber": "500000011-05-04-2016",
                        "CustNo": "",
                        "EntryDate": "20160504",
                        "CoDivision": "",
                        "Source": "OUTERBOXAPR2016",
                        "ProdDollars": 0,
                        "PHAmt": 0,
                        "TaxAmt": 0,
                        "Tax-Exempt-No": "",
                        "Tax-Exempt": "",
                        "LabelComment": "",
                        "DiscountComment": "",
                        "HeaderStatus": "",
                        "HeaderStatCd": "",
                        "RushProcessing": "N",
                        "PayMethod": "PP",
                        "EntireOrderInstallment": "",
                        "WaitForCustomer": "",
                        "AffiliateMarketing": {
                            "MarketingInfo": {
                                "Type": "",
                                "Affiliate": "",
                                "WebSite": "",
                                "Script": "",
                                "TVMedia": "",
                                "Number800": ""
                            }
                        }
                    }
                },
                "Coupons": {
                    "Coupon": {
                        "CouponType": "",
                        "DiscountCode": "",
                        "CouponNumber": "",
                        "DollarDiscountAmount": "",
                        "PercentageDiscount": ""
                    }
                },
                "Billing": {
                    "Billto": {
                        "OrderNumber": "500000011-05-04-2016",
                        "FirstName": "Jamie",
                        "MiddleInitial": "",
                        "LastName": "Fishback",
                        "CompanyName": "",
                        "Address1": "8903 WILLIS AVENUE",
                        "Address2": "WITB c\/o Conquest Digital",
                        "Address3": "Suite 9",
                        "City": "Los Angeles",
                        "State": "CA",
                        "ZipCode": "91402",
                        "CountryCd": "",
                        "Country": "US",
                        "DayPhone": "",
                        "Email": "producer@conquestdigital.tv"
                    }
                },
                "CreditCardInfo": {
                    "CreditCard": {
                        "CreditCardNumber": "",
                        "CreditCardExpDate": "",
                        "CreditCardType": "",
                        "CreditCardSecure": "",
                        "CreditCardAuthorization": "",
                        "RefNumber": "",
                        "AuthSource": "",
                        "AuthChar": "",
                        "TranID": "",
                        "ValidationCode": "",
                        "TransactionStatus": "",
                        "LTTranID": "",
                        "OP-TranID": "",
                        "OP-MerchantRefNumber": "",
                        "AN-TranID": "",
                        "AN-InvoiceNumber": "",
                        "CyberSource-TransID": "",
                        "EC-RoutingNumber": "",
                        "EC-BankAccount": ""
                    }
                },
                "CheckInfo": {
                    "Check": {
                        "RemitAmount": 0,
                        "CheckNumber": "",
                        "BankAccount": "",
                        "RoutingNumber": ""
                    }
                },
                "ProductManifest": {
                    "Product": [
                        {
                            "OrderNumber": "500000011-05-04-2016",
                            "LineNo": "",
                            "ItemNo": "E0044-TW-E72",
                            "Status": "",
                            "StatCd": "",
                            "Amount": "",
                            "Quantity": 1,
                            "Description": "",
                            "Ship-Meth-Cd": 24,
                            "BillTable": ""
                        }
                    ]
                },
                "Shipping": {
                    "Shipto": {
                        "OrderNumber": "500000011-05-04-2016",
                        "FirstName": "Jamie",
                        "MiddleInitial": "",
                        "LastName": "Fishback",
                        "CompanyName": "",
                        "Address1": "8903 WILLIS AVENUE",
                        "Address2": "WITB c\/o Conquest Digital",
                        "Address3": "Suite 9",
                        "City": "Los Angeles",
                        "State": "CA",
                        "ZipCode": "91402",
                        "CountryCd": "",
                        "Country": "US",
                        "DayPhone": ""
                    }
                }
            }
        }
    ]
}';

// Assign order data to orders POST variable
$data = array( 'orders' => $order_data);

// Submit our response
$response = $api->submitOrder($data);

echo json_encode($response, JSON_PRETTY_PRINT);