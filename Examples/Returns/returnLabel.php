<?php
/**
 * .php
 *
 * @author      Back In Black Inc.
 * @package     bibi_api_sdk
 * @copyright   2005-2020 Back In Back Solutions
 * @update      Last Updated by Larry 1/22/2020 2:24 PM
 * @link        http://bibisolutions.net
 * @version     ${VERSION}
 *
 */


// Display errors for testing purposes
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

define('PUBLIC_KEY', '');
define('SECRET_KEY', '');

require_once '../../bibi_php_sdk/bibi_sdk.php';

// Initialize the API with public and secret keys
$api = new Bibi_SDK(PUBLIC_KEY, SECRET_KEY);

// Authenticate and grab token
$tokenResponse = $api->Authenticate();

// Handle token response automatically
$api->HandleTokenResponse($tokenResponse);

$shipMethodsRequest = $api->listShipMethods();
$shipMethods = $shipMethodsRequest['result'];
if (isset($_POST['customer_order_number']) && $_POST['customer_order_number'] !== "") {
    $returnLabelResponse = $api->returnLabelOrder($_POST['customer_order_number'], $_POST['ship_method'], $_POST['weight']);
} elseif (isset($_POST['street']) && $_POST['street'] !== "") {
    $address_array = [
        'firstname' => $_POST['firstname'],
        'lastname' => $_POST['lastname'],
        'company' => $_POST['company'],
        'street' => $_POST['street'],
        'ref1' => $_POST['ref1'],
        'ref2' => $_POST['ref2'],
        'state' => $_POST['state'],
        'zip' => $_POST['zip'],
        'country_code' => $_POST['country_code'],
    ];
    $returnLabelResponse = $api->returnLabelAddress($address_array, $_POST['ship_method'], $_POST['weight']);
} else {
    $returnLabelResponse = false;
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet">
    <title>Return Label</title>
</head>

<body>

<div class="container">
    <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <h2>Create Return Labels</h2>
        </div>
    </div>
    <form method="post">
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                <div class="form-group">
                    <label for="customer_order_number" class="col-sm-2 control-label">Order Number</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" id="customer_order_number" placeholder="Customer_order_number"
                               value="<?php echo (isset($_POST['customer_order_number'])) ? $_POST['customer_order_number'] : ""; ?>">
                    </div>
                </div>
                <div class="form-group">
                    <label for="weight" class="col-sm-2 control-label">Weight</label>
                    <div class="col-sm-10">
                        <input type="number" class="form-control" id="weight" placeholder="Weight" name="weight"
                               value="<?php echo (isset($_POST['weight'])) ? (int)$_POST['weight'] : "0.00"; ?>">
                    </div>
                </div>
                <div class="form-group">
                    <label for="ship_method" class="col-sm-2 control-label">Ship_method</label>
                    <div class="col-sm-10">
                        <select class="form-control" id="ship_method" name="ship_method">
                            <option value="">Select a ShipMethod</option>
                            <?php
                            foreach ($shipMethods as $shipMethod) {
                                if (isset($_POST['ship_method']) && $shipMethod['code'] == $_POST['ship_method']) {
                                    echo '<option value="' . $shipMethod['code'] . '" SELECTED>' . $shipMethod['short'] . '</option>';
                                } else {
                                    echo '<option value="' . $shipMethod['code'] . '">' . $shipMethod['short'] . '</option>';
                                }
                            }
                            ?>
                        </select>
                    </div>
                </div>
            </div>

            <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                <div class="form-group">
                    <label for="firstname" class="col-sm-2 control-label">Firstname</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" id="firstname" placeholder="Firstname" name="firstname">
                    </div>
                </div>
                <div class="form-group">
                    <label for="lastname" class="col-sm-2 control-label">Lastname</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" id="lastname" placeholder="Lastname" name="lastname">
                    </div>
                </div>
                <div class="form-group">
                    <label for="company" class="col-sm-2 control-label">Company</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" id="company" placeholder="Company" name="company">
                    </div>
                </div>
                <div class="form-group">
                    <label for="street" class="col-sm-2 control-label">Street</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" id="street" placeholder="Street" name="street">
                    </div>
                </div>
                <div class="form-group">
                    <label for="ref1" class="col-sm-2 control-label">Ref1</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" id="ref1" placeholder="Ref1" name="ref1">
                    </div>
                </div>
                <div class="form-group">
                    <label for="ref2" class="col-sm-2 control-label">Ref2</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" id="ref2" placeholder="Ref2" name="ref2">
                    </div>
                </div>
                <div class="form-group">
                    <label for="state" class="col-sm-2 control-label">State</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" id="state" placeholder="State" name="state">
                    </div>
                </div>
                <div class="form-group">
                    <label for="zip" class="col-sm-2 control-label">Zipcode/PostalCode</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" id="zip" placeholder="Zipcode" name="zip">
                    </div>
                </div>
                <div class="form-group">
                    <label for="country_code" class="col-sm-2 control-label">Country Code</label>
                    <select class="form-control" id="country_code" name="country_code">
                        <option value="US">US</option>
                    </select>
                </div>
            </div>
        </div>
    </form>

    <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <?php
            if ($returnLabelResponse === false) {
                ?>
                <div class="hidden">No Label Created</div>
                <?php
            } else {
                echo '<img src="data:image/png;base64,' . $returnLabelResponse['result']['label'] . '" style="max-width: 400px;max-height: 600px;"/>';
            }
            ?>
        </div>
    </div>
</div>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
</body>
</html>