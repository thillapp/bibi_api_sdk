<?php

/*
* Docs:    http://www.bibisolutions.com/developers
* Version: 2.07
* Author:  BiBi Solutions Development Team
* Date:    2021-07-20
*/

/**
 * Class Bibi_SDK
 */
class Bibi_SDK
{
    public $endpoint = "";
    public $publicKey = "";
    public $secretKey = "";
    private $_prefix = "https://";
    private $_postfix = "@bibisolutions.net/api/";
    private $_token = "";

    /* Define public and secret keys in construct */

    /**
     * Bibi_SDK constructor.
     *
     * @param string $public
     * @param string $secret
     * @throws Exception
     */
    public function __construct($public = "", $secret = "")
    {
        if (!function_exists('curl_version')) {
            throw new Exception('cURL must be installed on this server to run the Bibi SDK.');
        }

        if (strlen($public) == 32 || $public == "")
            $this->publicKey = $public;
        else
            throw new Exception('Invalid public key - length must be 32.');

        if (strlen($secret) == 32 || $secret == "")
            $this->secretKey = $secret;
        else
            throw new Exception('Invalid secret key - length must be 32.');
    }

    /* Manually define public key */

    /**
     * Gets public key
     *
     * @return string
     */
    public function GetPublicKey()
    {
        return $this->publicKey;
    }

    /**
     * Sets Public Key
     *
     * @param string $key
     * @throws Exception
     */
    public function SetPublicKey($key)
    {
        if (strlen($key) == 32)
            $this->publicKey = $key;
        else
            throw new Exception('Invalid public key - length must be 32.');
    }

    /* Manually define secret key */

    /**
     * gets Secret Key
     *
     * @return string
     */
    public function GetSecretKey()
    {
        return $this->secretKey;
    }

    /**
     * Sets Secret Key
     *
     * @param string $key
     * @throws Exception
     */
    public function SetSecretKey($key)
    {
        if (strlen($key) == 32)
            $this->secretKey = $key;
        else
            throw new Exception('Invalid secret key - length must be 32.');
    }

    /* Authenticate to grab a token */

    /**
     * Authenticates Access
     *
     * @return bool|string
     */
    public function Authenticate()
    {
        $this->endpoint = $this->_prefix . $this->publicKey . ':' . $this->secretKey . $this->_postfix;
        return $this->Command();
    }

    /* Set or get token */

    /**
     * Creates a CURL request and posts the data
     *
     * @param string $api API endpoint
     * @param array|null $data Data values
     * @return bool|string Result
     */
    private function Command($api = "", $data = null)
    {
        $data_string = '';
        if ($data) {
            // Convert POST data into URL format
            foreach ($data as $key => $value) {
                $data_string .= $key . '=' . $value . '&';
            }
            rtrim($data_string, '&');
        }
        // Open connection
        $ch = curl_init();
        // Set the url, # of POST vars, POST data
        curl_setopt($ch, CURLOPT_URL, $this->endpoint . $api);
        curl_setopt($ch, CURLOPT_POST, (isset($data) ? 1 : 0));
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        // Execute CURL command
        $result = curl_exec($ch);
        // Close connection
        curl_close($ch);
        return $result;
    }

    /**
     * Gets Token
     *
     * @return string Token from BIBI API
     */
    public function GetToken()
    {
        return $this->_token;
    }

    /* Handle the token response */

    /**
     * Sets Token for later use
     *
     * @param string $token Token from BIBI API
     * @return bool
     * @throws Exception
     */
    public function SetToken($token)
    {
        if (strlen($token) == 64) {
            $this->_token = $token;
            $this->endpoint = $this->_prefix . $this->publicKey . ':' . $this->_token . $this->_postfix;
            return true;
        } else {
            throw new Exception('Invalid token - length must be 64.');
        }
    }

    /**
     * Process to take getToken response and set it for later use
     *
     * @param string $response
     * @return string|bool
     * @throws Exception
     */
    public function HandleTokenResponse(string $response)
    {
        $response = json_decode($response);
        if ($response->success) {
            $this->SetToken($response->token);
            return true;
        } else {
            // Failure
            throw new Exception($response->message);
        }
    }

    /* CURL command */
    /**
     * Request to API
     *
     * @param string $method API Endpoint
     * @param array $arguments Data to post to endpoint
     * @return mixed
     */
    public function __call($method, $arguments)
    {
        // This allows us to attempt to hit API endpoints if the method doesn't exist in our SDK
        if (!method_exists($this, $method)) {
            if ($method == 'listProduct' && isset($arguments[0])) {
                $method = 'listProduct/' . $arguments[0];
                $arguments = [];
            }
            if (empty($arguments))
                return json_decode($this->Command($method, null));
            else
                return json_decode($this->Command($method, $arguments[0]));
        } else {
            // Otherwise default to original request
            return call_user_func_array([$this, $method], $arguments);
        }
    }

    /**
     * List of returns based on date
     *
     * @param string|array $param1 if string that is the start date if array date_start & date_end are used from array
     * @param string $date_end end date, if not used then the end of the current day is used
     * @return array
     */
    public function listReturnsByDate($param1, $date_end = '')
    {
        $date_end = ($date_end == '') ? date("Y-m-d") . ' 23:59:59' : $date_end;
        if (is_array($param1)) {
            $date_start = $param1['date_start'];
            $date_end = (!isset($param1['date_end']) || $param1['date_end'] == '') ? date("Y-m-d") . ' 23:59:59' : $param1['date_end'];
        } else {
            $date_start = $param1;
        }
        $data = ['date_start' => $date_start, 'date_end' => $date_end];
        if (isset($param1['offset'])) $data['offset'] = $param1['offset'];
        if (isset($param1['perPage'])) $data['perPage'] = $param1['perPage'];
        $response = $this->apiJsonRequest('listReturns', $data);
        return $response;
    }

    // Returns

    /**
     * request API with json payload
     *
     * @param string $api API endpoint
     * @param array $data data to json_encode to send to BIBI API
     * @return array
     */
    private function apiJsonRequest($api, $data = '')
    {
        $url = $this->endpoint . $api;
        $payload = json_encode($data);
        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLINFO_HEADER_OUT, true);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $payload);
        curl_setopt($ch, CURLOPT_HTTPHEADER, [
                'Content-Type: application/json',
                'Content-Length: ' . strlen($payload)]
        );
        $result = curl_exec($ch);

        $return = json_decode($result, true);
        curl_close($ch);
        return $return;
    }

    /**
     * List of returns on order
     *
     * @param string $order_id customer_order_number
     * @return array
     */
    public function listReturnsByOrder($order_id)
    {
        $data = ['customer_order_number' => $order_id];
        $response = $this->apiJsonRequest('listReturns', $data);
        return $response;
    }

    /**
     * Get return label based on customer_order_number
     *
     * @param string $order_number customer_order_number of return
     * @param string $ship_method Optional but if used should be the shipping method code from listShipMethods
     * @param int|float $weight Optional but should be weight of return package
     * @return array
     */
    public function returnLabelOrder($order_number, $ship_method = "", $weight = 0.00)
    {
        $data = ['customer_order_number' => $order_number];
        if (isset($ship_method) && $ship_method !== "") $data['ship_method'] = $ship_method;
        if (isset($weight) && (float)$weight > 0) $data['weight'] = (float)$weight;
        $response = $this->apiJsonRequest('returnLabel', $data);
        return $response;
    }

    /**
     * Get return label without order_information
     *
     * @param array $address address array of from address for label
     * @param string $ship_method shipping method code from listShipMethods for return
     * @param float $weight Weight of return shipment
     * @return array
     */
    public function returnLabelAddress($address, $ship_method, $weight)
    {
        $data = ['address' => $address, 'ship_method' => $ship_method, 'weight' => $weight];
        $response = $this->apiJsonRequest('returnLabel',$data);
        return $response;
    }


    /**
     * @param string $order_number customer order number
     * @return array
     */
    public function holdOrder($order_number){
        $response = $this->apiJsonRequest('holdOrder/'.$order_number);
        return $response;
    }


    // ASNs


    /**
     * Adds a product to an ASN
     *
     * @param string $asn_number asn number as defined
     * @param array $product_details productDetails array
     * @return array
     */
    public function addASNProduct($asn_number, $product_details)
    {
        $data = $product_details;
        $data['asn_bol_track_number'] = $asn_number;
        $response = $this->apiJsonRequest('addASNProduct', $data);
        return $response;
    }

    /**
     * Creates an ASN
     *
     * @param array $header array of ASN header details
     * @param array $products Array of products arrays to have in ASN
     * @return array
     */
    public function createASN($header, $products)
    {
        $data = ['Header' => $header, 'Products' => $products];
        $response = $this->apiJsonRequest('addASNProduct', $data);
        return $response;
    }

    /**
     * Removes a product from an ASN
     *
     * @param string $asn_number asn number
     * @param string $product_number product number
     * @return array
     */
    public function deleteASNProduct($asn_number, $product_number)
    {
        $data = ['asn_bol_track_number' => $asn_number, 'product_number' => $product_number];
        $response = $this->apiJsonRequest('deleteASNProduct', $data);
        return $response;
    }

    /**
     * removes and ASN
     *
     * @param string $asn_number asn number
     * @return array
     */
    public function deleteASNs($asn_number)
    {
        $data = ['asn_bol_track_number' => $asn_number];
        $response = $this->apiJsonRequest('deleteASN', $data);
        return $response;
    }

    /**
     * Gets the details of an ASN     *
     *
     * @param string $asn_number asn number
     * @return array
     */
    public function getASNDetails($asn_number)
    {
        $data = ['asn_bol_track_number' => $asn_number];
        $response = $this->apiJsonRequest('getASNDetails', $data);
        return $response;
    }

    /**
     * Gets details of a product form an ASN
     *
     * @param string $asn_number asn number
     * @param string $product_number  product number
     * @return array
     */
    public function getASNProduct($asn_number, $product_number)
    {
        $data = ['asn_bol_track_number' => $asn_number, 'product_number' => $product_number];
        $response = $this->apiJsonRequest('getASNProduct', $data);
        return $response;
    }

    /**
     * List the current ASNs
     *
     * @return array
     */
    public function listASNs()
    {
        $response = $this->apiJsonRequest('listASNs');
        return $response;
    }

    /**
     * Update the status of an ASN
     *
     * @param string $asn_number updateASNStatus
     * @param string $status
     * @return array
     */
    public function updateASNStatus($asn_number, $status)
    {
        $data = ['asn_bol_track_number' => $asn_number, 'status' => $status];
        $response = $this->apiJsonRequest('updateASNStatus', $data);
        return $response;
    }

    // Products
    /**
     * Lists products
     *
     * @param array $parameters limitations on list of products
     * @return array
     */
    public function listProducts(array $parameters = [])
    {
        $response = $this->apiJsonRequest('listProducts', $parameters);
        return $response;
    }
    /**
     * Lists ShipMethods
     *
     * @return array
     */
    public function listShipMethods(){
        $response = $this->apiJsonRequest('listShipMethods');
        return $response;
    }

    /**
     * @param string $dateStart
     * @param string $dateEnd
     * @param string $productNumber
     * @param array $transactionType
     * @return array
     */
    public function inventoryTransactions(string $dateStart = '', string $dateEnd = '', string $productNumber = '', array $transactionType = [])
    {
        $parms = [];
        if($dateStart !== ''){
            $parms['date_start'] = $dateStart;
        }
        if($dateEnd !== ''){
            $parms['date_end'] = $dateEnd;
        }
        if($productNumber !== ''){
            $parms['product_number'] = $productNumber;
        }
        if(count($transactionType) > 0){
            $parms['transaction_type'] = $transactionType;
        }
        $response = $this->apiJsonRequest('inventory/transactions',$parms);
        return $response;
    }

    /**
     * @return array
     */
    public function inventoryTransactionTypes()
    {
        $response = $this->apiJsonRequest('inventory/transactionTypes');
        return $response;
    }
    public function inventoryResons()
    {
        $response = $this->apiJsonRequest('inventory/reasons');
        return $response;
    }
}

