<?php
/**
 * listStates.php
 *
 * @author      Back In Black Inc.
 * @package     bibisolutions_api_php
 * @copyright   2005-2018 Back In Back Solutions
 * @update      Last Updated by Larry Bislew 8/20/2018 4:01 PM
 * @link        http://bibisolutions.net
 * @version     2.04.1
 *
 */


// Display errors for testing purposes
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

define('PUBLIC_KEY', '');
define('SECRET_KEY', '');

require_once '../../bibi_php_sdk/bibi_sdk.php';

// Initialize the API with public and secret keys
$api = new Bibi_SDK(PUBLIC_KEY, SECRET_KEY);

// Authenticate and grab token
$tokenResponse = $api->Authenticate();

// Handle token response automatically
$api->HandleTokenResponse($tokenResponse);

// See List Country API (listCountries) for correct Country ISO Numeric Code
// or visit https://www.nationsonline.org/oneworld/country_code_list.htm
// 840 is USA.

$data['country_code'] = '840';
$response = $api->listStates($data);

echo json_encode($response, JSON_PRETTY_PRINT);