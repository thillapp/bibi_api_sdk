/*
* Docs:    http://www.bibisolutions.com/developers
* Version: 2.07.0
* Author:  Larry Bislew <larry.bislew@backinblackinc.com>
* Date:    2021-07-20
*/

CHANGES
===========================================================================================
- Add Inventory API Endpoints
    -- transactions
    -- transactionTypes
    -- reasons
- Converted array() to short syntax
- Added Hold Order API Endpoint


NOTES
============================================================================================

This SDK is provided as a sample/wrapper to utilize the Bibi API. The only file required is named: bibi_sdk.php

The SDK requires that cURL is installed on your server.

Please see the developer documentation if you have questions on how API calls are made. If 
you need the .NET SDK, there is a separate download for that as well.

============================================================================================


/Examples
============================================================================================

The examples are provided using PHP and shows usage on a few different APIs available at
Bibi. Please read the documentation thoroughly to see what we have to offer at:

http://www.bibisolutions.com/developers

============================================================================================

