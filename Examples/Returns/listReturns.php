<?php
/**
 * listReturns.php
 *
 * @author      Back In Black Inc.
 * @package     bibisolutions_api_php
 * @copyright   2005-2018 Back In Back Solutions
 * @update      Last Updated by Larry Bislew 8/20/2018 12:22 PM
 * @link        http://bibisolutions.net
 * @version     2.04.1
 *
 */

// Display errors for testing purposes
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

define('PUBLIC_KEY', '');
define('SECRET_KEY', '');

require_once '../../bibi_php_sdk/bibi_sdk.php';

// Initialize the API with public and secret keys
$api = new Bibi_SDK(PUBLIC_KEY, SECRET_KEY);

// Authenticate and grab token
$tokenResponse = $api->Authenticate();

// Handle token response automatically
$api->HandleTokenResponse($tokenResponse);


$data = array();
// For searching by Customer Order Number


?>
<h2>List Returns:</h2>
<br/>
<br/>
<h3>By Date Range</h3>
<pre>
    <?php

    // For Searching by Date
    $data['date_start'] = "2018-01-01 00:00:00";
    $data['date_end'] = "2018-12-31 23:59:59";
    $data['offset'] = 2;
    $data['perPage'] = 10;
    $response = $api->listReturnsByDate($data);
    echo json_encode($response, JSON_PRETTY_PRINT);
    ?>
    </pre>
<br/>
<h3>By Order Number</h3>
<pre>
    <?php

    $order_id = "8675309";
    $response = $api->listReturnsByOrder($order_id);
    echo json_encode($response, JSON_PRETTY_PRINT);
    ?>
    </pre>
